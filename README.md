
## Práctica de crecimiento personal

Desarrollo de una simple aplicación web, usando:

- Del lado del Cliente (Frontend) [Vue Js](https://vuejs.org/).
- Del lado del Servidor (Backend) [Laravel](https://laravel.com).
- Para crear interfaces dinámicas [Vuetify](https://vuetifyjs.com/en/).

## Pasos que seguí durante el desarrollo

1. Cree un proyecto laravel, como lo indica la [documentación](https://laravel.com/docs/7.x#installing-laravel)

2. Para trabajar con vue, es necesario requerir [Laravel/ui](https://laravel.com/docs/7.x/frontend#introduction)

3. Corri el comando correspondiente para que funcione vue ``` php artisan ui vue ```

4. Instalé e importé el framework Vuetify para efectos de maquetación

5. Utilicé VUE-ROUTER para la navegación.

6. Maquetación de crud para articulos y usuarios

7. Maquetación y funcionalidad del lado del cliente para roles y permisos
  Usando componentes de vuetify

8. Subir proyecto base a repositorio.

9. Creación de dos ramas, una llamada frontend y otra backend, para el continuo desarrollo.

10. Análisis de base de datos, se creo modelos, migraciones, factory y test unitarios.

11. Funcionalidad del login y permisos (Backend).

12. Integrar API de laravel a vue, usando Axios.
