import Vue from 'vue'
import VueRouter  from 'vue-router'

import Principal from "./views/Principal.vue";
import Dashboard from "./views/Dashboard.vue";
import Articulos from "./views/Articulos/Index.vue";
import Usuarios from "./views/Usuarios/Index.vue";
import Roles from "./views/Roles/Index.vue";
import Permisos from "./views/Roles/Permisos.vue";
import Perfil from "./views/Perfil.vue";

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Principal
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/articulos',
      name: 'articulos',
      component: Articulos
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: Usuarios
    },
    {
      path: '/roles',
      name: 'roles',
      component: Roles
    },
    {
      path: '/roles/:slug',
      name: 'permisos',
      component: Permisos
    },
    {
      path: '/mi-cuenta',
      name: 'mi-cuenta',
      component: Perfil
    },
    {
      path: '*',
      component: require('./views/404').default
    },
    {
      path: '/*',
      redirect: { name: 'dashboard' }
    }
  ],
  mode: 'history'
})
